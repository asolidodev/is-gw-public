# How to make a bootable USB device for the APU and install Ubuntu 16.04

# get the tinycore package from pc engines
# wget http://pcengines.ch/file/apu2-tinycore6.4.tar.bz2

# get the Ubuntu Xenial 16.04 modified files from pc engines
# wget http://pcengines.ch/tmp/ubuntu16.04_serial.tar.bz2

# get the MBR image file mbr.bin from the syslinux source package at:
# wget https://www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-4.04.tar.bz2
# (the file is located in: syslinux-4.04/mbr/mbr.bin)

# move the apu2-tinycore6.4.tar.bz2, ubuntu16.04_serial.tar.bz2 and mbr.bin into the FILESDIR directory (see below)

# enter root permissions
	sudo su -

# set these variables on your shell
	FILESDIR=/path/to/files
	DEVICE=dev/yourdevice

# partition and format
	umount ${DEVICE}1
	dd if=/dev/zero of=${DEVICE} count=1 conv=notrunc
	echo -e "o\nn\np\n1\n\n\nw" | fdisk ${DEVICE}
	mkfs.vfat -n XENIAL_APU -I ${DEVICE}1

# make the device bootable
	syslinux -i ${DEVICE}1
	dd conv=notrunc bs=440 count=1 if=$FILESDIR/mbr.bin of=${DEVICE}
	parted ${DEVICE} set 1 boot on

# unpack modified installers
	mount ${DEVICE}1 /mnt
	tar -C /mnt -xjf $FILESDIR/apu_tinycore.tar.bz2
	tar -C /mnt -xjf $FILESDIR/ubuntu16.04_serial.tar.bz2
	umount /mnt

# exit root and remove the flash drive, it should be ready for booting
#
#  
# Install the operating system from the USB drive at your liking.
# Make sure you DONT ALLOW login as root and that you install an OpenSSH server during the installation
# this way you will be able to login via SSH with the sudoer user created by the installer
# to enable serial console access, follow the ubuntu comunity instructions: https://help.ubuntu.com/community/SerialConsoleHowto
