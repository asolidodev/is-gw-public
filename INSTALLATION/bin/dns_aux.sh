echo "
options {
	directory \"/var/cache/bind\";

	forwarders {
		$DNS_FORWARDERS
	};

	auth-nxdomain no;
	listen-on-v6 { any; };
};" > ${DNS_NAMED_CONF_OPTIONS}_temp

