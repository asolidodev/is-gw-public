# $1: interface
# $2: static ip
# $3: network
# $4: netmask
# $5: broadcast
echo_static_iface() {
echo "
auto $1
iface $1 inet static
	address $2
	network $3
	netmask $4
	broadcast $5"
}


# $1: interface
# $2: static ip
# $3: network
# $4: netmask
# $5: broadcast
# $6: dns-nameservers
echo_static_iface_dns() {
echo "
auto $1
iface $1 inet static
	address $2
	network $3
	netmask $4
	broadcast $5
	dns-nameservers $6"
}

echo_wan_static_iface() {
echo "
auto $1
iface $1 inet static
	address $2
	network $3
	netmask $4
	broadcast $5
	gateway $6"
}

# $1: interface
# $2: static ip
# $3: network
# $4: netmask
# $5: broadcast
# $6: bridging interfaces
# $7: dns-nameservers
echo_bridge_iface() {

	if [ "$#" -ne 7 ]; then
		echo "Bad LAN config!"
		exit 1
	fi

echo "
auto $1
iface $1 inet static
	address $2
	network $3
	netmask $4
	broadcast $5
	dns-nameservers $(echo "${7}" | sed "s/-/ /g")
	bridge_ports $6
	bridge_stp off"
}


# $1: interface
# $2: static ip
# $3: netmask
# $4: vlan
# $5: broadcast
echo_static_iface_vlan(){
echo "

auto ${1}
iface ${1} inet static
	address $2
	netmask $3
	broadcast $5
	gateway $6
	vlan-raw-device $(echo "$1" | sed "s/\./ /g" | awk '{print $1 }')
	"
}

echo_wan_static_iface_vlan(){
echo "

auto ${1}
iface ${1} inet static
	address $2
	netmask $3
	broadcast $5
	gateway $6
	vlan-raw-device $(echo "$1" | sed "s/\./ /g" | awk '{print $1 }')
	"
}


# $1: interface
echo_dhcp_iface() {
echo "
auto $1
iface $1 inet dhcp"
}


# $1: interface
# $2: vlan
echo_dhcp_iface_vlan(){
echo "

auto ${1}
iface ${1} inet dhcp
	vlan-raw-device $(echo "$1" | sed "s/\./ /g" | awk '{print $1 }')
"
}
