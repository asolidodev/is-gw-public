#!/bin/bash

SOURCELIST="/etc/apt/security.sources.list"
SECURITYONLY=yes
SECOPTION=

if [ $SECURITYONLY="yes" ]; then
	SECOPTION="-o Dir::Etc::SourceList=$SOURCELIST"
fi

apt-get update >& /dev/null

LIST=`dpkg --get-selections | awk '{ if ($2 == "install")  print $1 }'`
SECLIST=

for p in $LIST; do
  # we sort by the version field (latest first) and retain only the first line, to support the rare cases where more than one pkg is there
  TEMP=`apt-cache madison $p $SECOPTION | grep Packages |  sort -k 2 -r |head -n 1| cut -d '|' -f 1,2`
  if [ ! -z "$TEMP" ]; then
    NAME=`echo $TEMP | cut -d '|' -f 1`
    VERSION=`echo $TEMP | cut -d '|' -f 2`
    CURRENT=`dpkg -l $p | tail -n +6 |awk '{ print $3 }'`
    dpkg --compare-versions $CURRENT lt $VERSION
    RC=$?
    if [ $RC -eq 0 ]; then
      SECLIST="$SECLIST $p"
    fi
  fi
done

# we remove the kernel updates as they are too risk to be done autmatically
CLEANLIST="`echo $SECLIST | sed -r  "s/linux-[^[:space:]]+/ /g"` "

# we remote held packages from the list even though apt-get install -y
# should refuse to do any operation that would touch them
HELDPACKAGES=`dpkg --get-selections |grep hold |cut -f 1`

# matching packagesname followed by a space or EOL to avoid partial matches
# we must match the EOL as well, as the last package isn't followed by a space
# same goes for the package at the beggining of the line
for p in $HELDPACKAGES; do 
 CLEANLIST=`echo $CLEANLIST | sed -r "s/^$p[[:space:]]|[[:space:]]$p[[:space:]]|[[:space:]]$p$/ /g"`
done

# this result can be piped to another command
echo $CLEANLIST
