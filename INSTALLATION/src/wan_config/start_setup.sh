#!/bin/bash

re='^[0-9]+$'

LOCAL_LIB_DIR="<LOCAL_LIB_DIR>"
LOCAL_ETC_DIR="<LOCAL_ETC_DIR>"

. ${LOCAL_ETC_DIR}/dynamic_vars --source-only


# use argument -b for wan backup configuration
if [ x$1 = "x-b" ]; then
    if [ x$WAN_BKP_METHOD = "xnone" ] || [ x$WAN_BKP_METHOD = "x" ];then
        echo; echo "Backup interface does not exist. Exiting..."; echo; sleep 1
        exit
    fi
fi

check_ip(){

    ip=$1

    if expr "$ip" : '[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$' >/dev/null; then
    for i in 1 2 3 4; do
        if [ $(echo "$ip" | cut -d. -f$i) -gt 255 ]; then
        return 1
        fi
    done
    return 0
    else
    return 1
    fi
}

export_vars(){
    export NEW_WAN_IFACE
    export NEW_WAN_UPSTREAM
    export NEW_WAN_METHOD
    export NEW_WAN_STATIC_IP
    export NEW_WAN_NETMASK
    export NEW_WAN_BROADCAST
    export NEW_WAN_NETWORK
    export NEW_WAN_GATEWAY
}

# ask for interface method
while true; do
    read -p "What is the method of the new interface? ( dhcp | static ) -> " NEW_WAN_METHOD
    case $NEW_WAN_METHOD in
        "dhcp" ) break;;
        "static" ) break;;
        * ) echo "Please insert a valid method.";;
    esac
done

# ask for iface name
while true; do
    echo
    read -p "What is the name of your new interface? -> " NEW_WAN_IFACE_TEMP
    case $NEW_WAN_IFACE_TEMP in
        "" ) echo; echo "WARNING: Please insert an interface name.";;
        * ) break;;
    esac
done

# ask for iface name
while true; do
    echo
    echo "What is your interface's vlan number?"
    read -p "If there is no vlan, just press enter'  -> " NEW_IFACE_VLAN_NUM
    if [[ x$NEW_IFACE_VLAN_NUM = "x" ]]; then
        NEW_WAN_IFACE=$NEW_WAN_IFACE_TEMP; break
    elif [[ $NEW_IFACE_VLAN_NUM =~ $re ]]; then
        NEW_WAN_IFACE="${NEW_WAN_IFACE_TEMP}.${NEW_IFACE_VLAN_NUM}"; break
    else
        echo; echo "WARNING: Please insert a valid vlan number."
    fi
    
done

# ask for upstream bandwidth
while true; do
    echo
    read -p "What is the bandwidth of the upstream in Mbit/s? -> " NEW_WAN_UPSTREAM
    if [[ $NEW_WAN_UPSTREAM =~ $re ]] && [ $NEW_WAN_UPSTREAM -ne 0 ];then
        break
    fi
    echo; echo "WARNING: Please insert a valid bandwidth."
done

if [ $NEW_WAN_METHOD = 'static' ];then

    # ask for iface IP
    while true; do
        echo
        read -p "What is the IP of your new interface? -> " NEW_WAN_STATIC_IP
        check_ip $NEW_WAN_STATIC_IP
        RES=$?
        if [ $RES -eq 1 ];then
            echo; echo "WARNING: Please insert a valid IP Address."
        elif [ $RES -eq 0 ];then
            break;
        fi
    done
    
    # ask for iface network
    while true; do
        echo
        read -p "What is the network of your new interface? -> " NEW_WAN_NETWORK
        check_ip $NEW_WAN_NETWORK
        RES=$?
        if [ $RES -eq 1 ];then
            echo; echo "WARNING: Please insert a valid network."
        elif [ $RES -eq 0 ];then
            break;
        fi
    done

    # ask for iface netmask
    while true; do
        echo
        read -p "What is the netmask of your network? -> " NEW_WAN_NETMASK
        check_ip $NEW_WAN_NETMASK
        RES=$?
        if [ $RES -eq 1 ];then
            echo; echo "WARNING: Please insert a valid network."
        elif [ $RES -eq 0 ];then
            break;
        fi
    done
    
    # ask for broadcast
    while true; do
        echo
        read -p "What is the broadcast IP for this new network? -> " NEW_WAN_BROADCAST
        check_ip $NEW_WAN_BROADCAST
        RES=$?
        if [ $RES -eq 1 ];then
            echo; echo "WARNING: Please insert a valid broadcast IP"
        elif [ $RES -eq 0 ];then
            break;
        fi
    done
    
    # ask for gateway
    while true; do
        echo
        read -p "What is the gateway IP for this new network? -> " NEW_WAN_GATEWAY
        check_ip $NEW_WAN_GATEWAY
        RES=$?
        if [ $RES -eq 1 ];then
            echo; echo "WARNING: Please insert a valid gateway IP"
        elif [ $RES -eq 0 ];then
            break;
        fi
    done
    
    echo
    echo These are your new configuration values:
    echo "  Interface           ->  $NEW_WAN_IFACE"
    echo "  Upstream bandwidth  ->  $NEW_WAN_UPSTREAM Mbit/s"
    echo "  Interface method    ->  $NEW_WAN_METHOD"
    echo "  Interface IP        ->  $NEW_WAN_STATIC_IP"
    echo "  Network             ->  $NEW_WAN_NETWORK"
    echo "  Netmask             ->  $NEW_WAN_NETMASK"
    echo "  Broadcast           ->  $NEW_WAN_BROADCAST"
    echo "  Gateway             ->  $NEW_WAN_GATEWAY"
    
elif [ $NEW_WAN_METHOD = 'dhcp' ];then
    
    echo
    echo These are your new configuration values:
    echo "  Interface           ->  $NEW_WAN_IFACE"
    echo "  Upstream bandwidth  ->  $NEW_WAN_UPSTREAM Mbit/s"
    echo "  Interface method    ->  $NEW_WAN_METHOD"
fi

while true; do
    echo
    read -p "Are these values correct? (yes | no) " yn
    case $yn in
        "Yes" | "yes" | "YES" ) export_vars; ${LOCAL_LIB_DIR}/wan_config/main_logic.sh $1; break;;
        "No" | "no" | "NO") exit;;
        * ) echo "Please answer yes or no.";;
    esac
done





